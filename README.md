Alpha version of this module, use for testing mostly!

How to use:
1) Make an actor and give it a transparant texture.
2) drag actor to canvas and select its token.
3) choose the rotate icon from the left side bar.
4) first load in a texture (down arrow icon) -- a name of the file will be displayed.
5) second set the size, 1 is a single grid square.
6) apply the texture by using the place button.
7) further options are under the options button, leave those for now.
8) close the dialog and click the rotat icon from the left side bar again, now there will be two options.
9) choosing the second opens a dialog where you can rotate your texture in different directions.
10) Profit! note: setting loop can cause issues with reloading by either client or you. Only use when everyone is on the same scene (for now.)

Thanks for trying out this module.
Freeze

class RotateTexture {
	
	static ready() {		
		game.socket.on ("module.rotate-texture", async data => {
			if(data.place){
				doPlacement(data.tokenId);
			};
			if(data.animate){
				doAnimation(data.tokenId);
			};
			if(data.remove){
				doRemove(data.tokenId);
			};
		});
	};
	static getSceneControlButtons(buttons) {
		let tokenButton = buttons.find(b => b.name == "token")

		if (tokenButton) {
			tokenButton.tools.push({
				name: "rt-controls",
				title: "Rotate Texture controls",
				icon: "fas fa-sync-alt",
				visible: game.user.isGM,
				onClick: () => RotateTexture.startDialog(),
				button: true
			});
		}
	};

	/**
	 * A startDialog() to get the ball rolling. What does it need to do:
     * 1) open 2 different dialogs, one to place stuff and one to animate stuff (this should only appear if the selected token has data).
	 */
	static async startDialog() {
		let tokens = canvas.tokens.controlled;
		if(tokens.length === 0) return ui.notifications.info("please select at least one token");
		let setTokens = [];
		let unsetTokens = [];
		for(let t of tokens) {
			const settings = await t.document.getFlag("rotate-texture", "settings") //!== undefined ? setTokens.push(t) : unsetTokens.push(t);
			if(!!settings && !!settings.imgPath){
				setTokens.push(t)
			}
			else unsetTokens.push(t);
		}
		let buttons = {
			place:{
				label: "Place/remove Texture",
				callback: () => {
					RotateTexture.placeDialog(setTokens, unsetTokens);
				}
			}
		}
		if(setTokens.length > 0){
			buttons.animate = {
				label: "Animate Texture",
				callback: () => {
					RotateTexture.animateDialog(setTokens);
				}
			}
		}
		new Dialog({
			title: "Rotate Textures:",
			content: "",
			buttons,
		}).render(true);
	};
	/**
	 * Animation dialog.
	 * @param {array} tokens 
	 */
	static async animateDialog(tokens) {
		let sortedTokens = tokens.sort((a,b) => { return a.name.split(/_|-/)[1] - b.name.split(/_|-/)[1] });
		let dialogAnimateContent = ``;
        let checked = "";
		for(let t of sortedTokens) {
            checked = t.document.getFlag("rotate-texture", "settings").loop ? "checked" : "";
			dialogAnimateContent += `
			<form>
            <div class="form-group row">
				<label>${t.name}: </label>
				<button class="dbutton cw-button90"   onclick="game.rotatetexture.animateTexture({tokenId: '${t.id}', angle:   90})" type="button"></button>
				<button class="dbutton acw-button90"  onclick="game.rotatetexture.animateTexture({tokenId: '${t.id}', angle:  -90})" type="button"></button>
				<button class="dbutton cw-button180"  onclick="game.rotatetexture.animateTexture({tokenId: '${t.id}', angle:  180})" type="button"></button>
				<button class="dbutton acw-button180" onclick="game.rotatetexture.animateTexture({tokenId: '${t.id}', angle: -180})" type="button"></button>
				<button class="dbutton cw-button360"  onclick="game.rotatetexture.animateTexture({tokenId: '${t.id}', angle:  360})" type="button"></button>
				<button class="dbutton acw-button360" onclick="game.rotatetexture.animateTexture({tokenId: '${t.id}', angle: -360})" type="button"></button>
                <label>Speed: </label>
                <input class="token-${t.id}" type="range" min="1" max="10" step="0.1" value="6"/>
                <label>Loop: </label>
                <input class="token-${t.id}" type="checkbox" onclick="game.rotatetexture.cancelAnimation('${t.id}')" ${checked}/>
			</div>
            </form>
			`;
		}
		new Dialog({
			title: "Control Panel",
			content: dialogAnimateContent,
			buttons: {}
		},
		{
			id: "rotate-texture-dialog" // due for a change to be cleaner with ids.
		}).render(true);
	}	
	/**
	 * Place and delete dialog.
	 * @param {array} setTokens array of tokens that already have their settings, will recieve basic settings here, including a randomID.
	 * @param {array} unsetTokens array of tokens that have not gotten their settings yet.
	 */
	static async placeDialog(setTokens, unsetTokens) {
		let tokens = [];
		for(let unsetT of unsetTokens) {
			await unsetT.document.setFlag("rotate-texture", "settings", {
				imgPath: "", 
				imgName: "", 
				size: 1, 
				alpha: 1, 
				markerId: randomID(), 
				grid: {w: canvas.grid.w, h: canvas.grid.h, size: canvas.grid.size},
				loop: false,
				location: {x: 0.5, y: 0.5},
				placed: false,
				angle: 0,
				anchor: {x: 0.5, y: 0.5}
			});
			tokens.push(unsetT);
		}
		for(let setT of setTokens) {
			tokens.push(setT);
		}
		tokens.sort((a,b) => { return a.name.split(/_|-/)[1] - b.name.split(/_|-/)[1] });
		let buttonRow = "";
		for(let t of tokens) {
			const tokenSettings = await t.document.getFlag("rotate-texture", "settings");
			buttonRow += `
                <form>
					<div class="form-group row">
						<label>${t.name}: </label>
						<button class="small-dbutton open-browser" onclick="game.rotatetexture._imageBrowser('${"input#" + t.name + "_input-name.input-name"}', '${t.id}')" type="button"></button>
						<input class="input-name" id="${t.name + "_input-name"}" value="${tokenSettings.imgName}" />
						<input class="input-size" id="${t.name + "_input-size"}" value="${tokenSettings.size}" />
						<button class="dbutton apply-texture" onclick="game.rotatetexture._placeTexture('${t.id}')" type="button"></button>
						<button class="dbutton options-texture" onclick="game.rotatetexture._optionsDialog('${t.id}')" type="button"></button>
						<button class="dbutton remove-texture" onclick="game.rotatetexture._removeTexture('${t.id}')" type="button"></button>	
					</div>
                </form>`;
		}
		let dialogCreateContent = `
		<form>
			${buttonRow}
		</form>
		`;
		new Dialog({
			title: "Control Panel",
			content: dialogCreateContent,
			buttons: {}
		},
		{
			id: "rotate-texture-dialog" // due for a change to be cleaner with ids.
		}).render(true);
	};
	/**
	 * Here we do some magic to get the file path of the image and set the correct name in the input.
	 * @param {string} element 
	 * @param {string} tokenId 
	 */
	static async imageBrowser(element, tokenId) {
		let pickedFile = await new FilePicker({
			type: "imagevideo",
			current: "/modules/rotate-texture/images/textures/",
			callback: async (path) => {
				let name = path.split("/").pop().split(".")[0];
				let token = canvas.tokens.get(tokenId);
				await token.document.setFlag("rotate-texture", "settings.imgPath", path);
				await token.document.setFlag("rotate-texture", "settings.imgName", name);
				$(element)[0].value = name;
			}
		});
		pickedFile.browse();
	};
	/**
	 * Settings for placement, alpha and size of the texture.
	 * @param {string} tokenId 
	 */
	static async optionsDialog(tokenId){
		const token = canvas.tokens.get(tokenId);
		let settings = duplicate(token.document.getFlag("rotate-texture", "settings"));
		if(!settings.placed) return ui.notifications.info("No texture has been placed yet on this token.")
		// what fields can be here, alpha setting... location... size (possibly remove from main window...)
		// content should be a table with left the options and right the values. Defaults should be preset in settings.
		//two buttons, a "Apply" and "Apply to all" button.
		let optionsDialogContent = `<h2>Texture options:</h2>
								<form>
                                    <div class="form-group row">
										<label>Size (multiples of token size):</label>
										<input class="options-input" id="textureSize"value="${settings.size}" />
									</div>
									<hr>
									<div class="form-group row">
										<label>X-location: </label>
										<input class="options-input" id="textureLocationX" value="${settings.location.x}" placeholder="0.5" />
									</div>
									<div class="form-group row">
										<label>Y-location: </label>
										<input class="options-input" id="textureLocationY" value="${settings.location.y}" placeholder="0.5" />
									</div>
									<hr>
									<div class="form-group row">
										<label>Alpha (0-1): </label>
										<input class="options-input" id="textureAlpha" value="${settings.alpha}" placeholder=1 "/>
									</div>
									<hr>
									<div class="form-group row">
										<label>X-Animation anchor: </label>
										<input class="options-input" id="textureAnchorX" value="${settings.anchor.x}" placeholder="0.5" />
									</div>
									<div class="form-group row">
										<label>Y-Animation anchor: </label>
										<input class="options-input" id="textureAnchorY" value="${settings.anchor.y}" placeholder="0.5" />
									</div>
                                </form>
								`;
		new Dialog({
			title: "Options Dialog",
			content: optionsDialogContent,
			buttons: {
				apply: {
					label: "Apply",
					callback: async (html) => {
						settings.location.x = parseFloat(html.find("#textureLocationX")[0].value);
						settings.location.y = parseFloat(html.find("#textureLocationY")[0].value);
						settings.size = parseFloat(html.find("#textureSize")[0].value);
						settings.alpha = parseFloat(html.find("#textureAlpha")[0].value);
						settings.anchor.x = parseFloat(html.find("#textureAnchorX")[0].value);
						settings.anchor.y = parseFloat(html.find("#textureAnchorY")[0].value);
						doRemove(tokenId)
						await token.document.setFlag("rotate-texture", "settings", settings);
						doPlacement(tokenId)
						game.socket.emit("module.rotate-texture", {tokenId, remove: true});
						game.socket.emit("module.rotate-texture", {tokenId, place: true})
					}
				},
				applyAll: {
					label: "Apply to all",
					callback: async (html) => {
						let tokens = canvas.tokens.controlled;
						for(let t of tokens) {
							let tempS = t.document.getFlag("rotate-texture", "settings");
							if(tempS === undefined) continue;
							let s = duplicate(tempS);
							if(!s.placed) continue;
							s.location.x = parseFloat(html.find("#textureLocationX")[0].value);
							s.location.y = parseFloat(html.find("#textureLocationY")[0].value);
							s.size = parseFloat(html.find("#textureSize")[0].value);
							s.alpha = parseFloat(html.find("#textureAlpha")[0].value);
							s.anchor.x = parseFloat(html.find("#textureAnchorX")[0].value);
							s.anchor.y = parseFloat(html.find("#textureAnchorY")[0].value);
							doRemove(t.id)
							await t.document.setFlag("rotate-texture", "settings", s);
							doPlacement(t.id)
							game.socket.emit("module.rotate-texture", {tokenId: t.id, remove: true});
							game.socket.emit("module.rotate-texture", {tokenId: t.id, place: true})
						}
					}
				}
			}
		},
		{
			width: 450,
			id: "rotation-options-dialog"
		}).render(true);
	};
	/**
	 * places the texture for the GM and the players.
	 * @param {string} tokenId is a tokenId. 
	 */
	static async placeTexture(tokenId) {
		const token = canvas.tokens.get(tokenId);
		let settings = duplicate(token.document.getFlag("rotate-texture", "settings"));
		if(settings.placed) return ui.notifications.info("texture already placed");
		const element = `${"input#" + token.name + "_input-size.input-size"}`;
		const size = $(element)[0].value;
		settings.size = parseFloat(size);
		settings.placed = true;
		await token.document.setFlag("rotate-texture", "settings", settings)
		doPlacement(tokenId);
		game.socket.emit("module.rotate-texture", {tokenId, place: true});

	};
	/**
	 * deletes the texture for the GM and the players.
	 * @param {string} tokenId is a tokenId. 
	 */
	static async removeTexture(tokenId) {
		doRemove(tokenId);
		const token = canvas.tokens.get(tokenId);
		let settings = duplicate(token.document.getFlag("rotate-texture", "settings"));
		settings.imgName = "";
		settings.imgPath = "";
		settings.placed = false;
		settings.size = 1;
		settings.angle = 0;
		settings.alpha = 1;
		settings.location = {x: 0.5, y: 0.5};
		settings.loop = false;
		game.socket.emit("module.rotate-texture", {tokenId, remove: true});
		await token.document.setFlag("rotate-texture", "settings", settings);
		$(`${"input#" + token.name + "_input-name.input-name"}`)[0].value = "";
		$(`${"input#" + token.name + "_input-size.input-size"}`)[0].value = 1;
	};
	/**
	 * Animates the texture for the GM and the players.
	 * @param {object} arg comes from the animationDialog, contains tokenId and angle. Speed and loop are found via jquery.
     * Function stores the variables in a flag on the token and then allows for animation of that token's layer. Via socket emit the player recieves the same job.
	 */
	static async animateTexture(arg, loop=false, speed=3) {
		const token = canvas.tokens.get(arg.tokenId);
		let settings = duplicate(token.document.getFlag("rotate-texture", "settings"));
		const markerId = settings.markerId;
		let markerToken = token.children.find(c => c.id === markerId);
		let angle = (markerToken.angle + arg.angle);
		settings.angle = angle;
		settings.loop = $('input[type=checkbox].token-' + token.id).length !== 0 ? $('input[type=checkbox].token-' + token.id)[0].checked : loop;
		settings.speed = $('input[type=range].token-' + token.id).length !==0 ? parseFloat($('input[type=range].token-' + token.id)[0].value) : speed;
		await token.document.setFlag("rotate-texture", "settings", settings);
		doAnimation(token.id)
		game.socket.emit("module.rotate-texture", {tokenId: token.id, animate: true});
	};
    /**
     * stops the one animation from looping.
     * @param {string} tokenId 
     */
    static async cancelAnimation(tokenId){
        const token = canvas.tokens.get(tokenId);
        let settings = duplicate(token.document.getFlag("rotate-texture", "settings"));
        if(!!settings.loop) {
            settings.loop = false;
            await token.document.setFlag("rotate-texture", "settings", settings);
        }
    };
    /**
     * stops all animations that are looping from looping.
     */
    static async cancelAllAnimation(){
        for(let token of canvas.tokens.placeables){
            if(!token.data.flags.hasOwnProperty("rotate-texture")) continue;
            let settings = duplicate(token.document.getFlag("rotate-texture", "settings"));
            if(!!settings.loop) {
                settings.loop = false;
                await token.document.setFlag("rotate-texture", "settings", settings);
            }
        }
    };
};
Hooks.on("init", function () {
	console.log("Initializing 'Rotate Textures'");
	RotateTexture.ready();
    game.rotatetexture = { 
        startDialog:        RotateTexture.startDialog,
        _optionsDialog:     RotateTexture.optionsDialog,
        _placeTexture:      RotateTexture.placeTexture,
        _removeTexture:     RotateTexture.removeTexture,
        animateTexture:     RotateTexture.animateTexture,
        cancelAnimation:    RotateTexture.cancelAnimation,
        cancelAllAnimation: RotateTexture.cancelAllAnimation,
        _imageBrowser:      RotateTexture.imageBrowser        
    };
});
Hooks.on('getSceneControlButtons', RotateTexture.getSceneControlButtons)

Hooks.on("canvasReady", async function (){
	let tokens = canvas.tokens.placeables;
	for(let t of tokens){
		const settings = t.document.getFlag("rotate-texture", "settings");
		if(!!settings && settings.placed){
			await doPlacement(t.id)
		}
	}
});

async function doPlacement(tokenId) {
	const token = canvas.tokens.get(tokenId);
	const settings = token.document.getFlag("rotate-texture", "settings")
	const {markerId, grid, imgPath, alpha, size, angle, anchor, location} = settings;
	let markerTexture = await loadTexture(imgPath);
	const textureSize = await grid.size * token.data.height;
	markerTexture.orig = { height: textureSize * size, width: textureSize * size, x: (textureSize * size) * location.x, y: (textureSize * size) * location.y };
	let sprite = new PIXI.Sprite(markerTexture)
    if(imgPath.includes("webm")) {
        sprite.texture.baseTexture.resource.source.loop = true;
        sprite.texture.baseTexture.resource.source.play();
    }
	sprite.anchor.set(anchor.x, anchor.y);
	let markerToken = token.addChild(sprite)
	token.sortableChildren = true
	markerToken.zIndex = game.user.isGM ? -1 : 100;
	markerToken.transform.position.set(textureSize * location.x, textureSize * location.y)
	markerToken.id = markerId;
	markerToken.alpha = alpha;
	markerToken.angle = angle;
}	
function doRemove(tokenId) {
	const token = canvas.tokens.get(tokenId);
	let { markerId } = token.document.getFlag("rotate-texture", "settings");
	const markerToken = token.children.find(c => c.id === markerId);
	token.removeChild(markerToken);
}
function doAnimation(tokenId){
    const token = canvas.tokens.get(tokenId);
    let settings = token.document.getFlag("rotate-texture", "settings");
    let start = 0;
    const time = (11 - settings.speed) * 1000; //ms for a full circle
    const frames = 60 * time / 1000;
    const markerId = settings.markerId;
    let markerToken = token.children.find(c => c.id === markerId);
    const rotation = settings.angle - markerToken.angle;
    function step(timestamp) {
        if (start === 0) start = timestamp;
        const elapsed = timestamp - start;
        markerToken.angle = rotation > 0 ? 
                Math.min(markerToken.angle + (rotation / (frames * (Math.abs(rotation)/ 360))), settings.angle) : 
                Math.max(markerToken.angle + (rotation / (frames * (Math.abs(rotation)/ 360))), settings.angle);
        if (elapsed < time * Math.abs(rotation)/ 360 ) {
            requestAnimationFrame(step);
        }
        else {
            const loop = token.document.getFlag("rotate-texture", "settings").loop;
            if(loop) {
                settings.angle += rotation;
                start = 0;
                requestAnimationFrame(step);
            }
        }
    }
    requestAnimationFrame(step);
}